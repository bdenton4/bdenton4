My name is Jonathan Yoder and I will graduate in December with dual degrees in Computer Science and Biosystems Engineering.
I enjoy coding, but I am most interested in using code as a tool to solve engineering problems.
I have spent the past few summers working with a professor on the Ag Campus on a system for monitoring tractors as they harvest seed corn.
This project involved both simple coding on Arduinos and more complex data processing, which was done in Java.